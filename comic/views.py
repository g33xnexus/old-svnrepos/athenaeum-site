from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from athenaeum.comic.models import Strip, Book, Comment, User, News, Gallery, GalleryImage, Ad, Progress
import datetime, sha, random, os

def index (request, strip_id = 0):

	gallery_new = ""

	news_new = ""
	
	strip_list_latest = Strip.objects.order_by ('-id')

	for stripL in strip_list_latest:
		if (stripL.date <= datetime.date.today()):
			latest = stripL
			break

	if (strip_id == 0):
		strip = latest
	
	else:
		strip = Strip.objects.get(id = strip_id)
	
	first = Strip.objects.all ()[0]
	
	previd = strip.id - 1
	
	if (previd < first.id):
		prev = first
	
	else: 
		prev = Strip.objects.get (id = previd)
	
	nextid = strip.id + 1

	if (nextid > latest.id):
		next = latest
	
	else: 
		next = Strip.objects.get (id = nextid)

	# Get a list of all books
	Books = Book.objects.all()
	
	comment_list = []
	
	s_list = strip.comments.split(',')
	for cid in s_list:
		if (cid != ''):
			comment_list.append(Comment.objects.get(id=cid))
	
	Comments = "True"

	if (comment_list == []):
		Comments = "False"

	Nnew = new_news()
	Gnew = new_gallery()
	user = logged_in(request)

	# Grab a random comic ad
	ad = None
	rnd = random.Random()
	if ( len(Ad.objects.all()) != 0):
		rnd_num = rnd.randint(1, Ad.objects.order_by('-id')[0].id)
		ad = Ad.objects.get(id = rnd_num)

	abs_id = Strip.objects.order_by('-id')[0].id

	if (latest.id < abs_id):
		stripNext = Strip.objects.get(id=(latest.id + 1))
	else:
		stripNext = latest

	progress = Progress.objects.get (strip = stripNext)
	return render_to_response('comic.html', {
			'user':			user,
            'strip':		strip,
            'stripNext':	stripNext,
            'first':		first,
            'latest':		latest,
            'prev':			prev,
            'next':			next,
			'Books':		Books,
			'curbook':		strip.book,
			'comment_list':	comment_list,
			'Comments':		Comments,
			'Nnew':			Nnew,
			'Gnew':			Gnew,
			'ad':			ad,
			'Progress':		progress,	
			})

def news (request):

	News_list = News.objects.order_by ('-id')

	Nnew = new_news()
	Gnew = new_gallery()
	user = logged_in(request)

	return render_to_response('news.html', {
			'user':			user,
            'News_list':	News_list,
			'Nnew':			Nnew,
			'Gnew':			Gnew,
			})

def characters (request):

	Nnew = new_news()
	Gnew = new_gallery()
	user = logged_in(request)

	return render_to_response('characters.html', {
			'user':		user,
			'Nnew':		Nnew,
			'Gnew':		Gnew,
			})

def archives (request):

	Books = Book.objects.order_by ('-id')
	Strips = Strip.objects.filter(date__lte=datetime.date.today())

	Nnew = new_news()
	Gnew = new_gallery()
	user = logged_in(request)

	return render_to_response('archives.html', {
			'user':		user,
			'Nnew':		Nnew,
			'Gnew':		Gnew,
			'Books':	Books,
			'Strips':	Strips,
			})

def about (request):

	Nnew = new_news()
	Gnew = new_gallery()
	user = logged_in(request)

	return render_to_response('about.html', {
			'user':		user,
			'Nnew':		Nnew,
			'Gnew':		Gnew,
			})

def links (request):

	Nnew = new_news()
	Gnew = new_gallery()
	user = logged_in(request)

	return render_to_response('links.html', {
			'user':		user,
			'Nnew':		Nnew,
			'Gnew':		Gnew,
			})

def gallery (request):

	Nnew = new_news()
	Gnew = new_gallery()
	user = logged_in(request)
	
	GalleryList = Gallery.objects.all()
	ImageList = GalleryImage.objects.all()

	return render_to_response('gallery.html', {
			'user':		user,
			'Nnew':			Nnew,
			'Gnew':			Gnew,
			'GalleryList':	GalleryList,
			'ImageList':	ImageList,
			})

def gallery_image (request, image_id = 0):

	Nnew = new_news()
	Gnew = new_gallery()
	user = logged_in(request)

	image = GalleryImage.objects.get(id = image_id)
	
	comment_list = []
	
	gi_list = image.comments.split(',')
	for cid in gi_list:
		if (cid != ''):
			comment_list.append(Comment.objects.get(id=cid))
	
	Comments = "True"

	if (comment_list == []):
		Comments = "False"

	return render_to_response('gallery_image.html', {
			'user':			user,
			'Nnew':			Nnew,
			'Gnew':			Gnew,
			'image':		image,
			'comment_list':	comment_list,
			'Comments':		Comments,
			})


def admin (request, strip_id = 0):

	if (request.session.get('User') != None):
		Nnew = new_news()
		Gnew = new_gallery()
		user = logged_in(request)

		Books = Book.objects.order_by ('-id')
		Strips = Strip.objects.order_by ('-id')
		News_list = News.objects.order_by ('-id')
		Gallerys = Gallery.objects.order_by ('-id')
		Images = GalleryImage.objects.order_by ('-id')
		if (strip_id != 0):
			delstrip (strip_id)
			return HttpResponseRedirect(reverse('athenaeum.comic.views.admin'))
		return render_to_response('admin.html', {
				'user':		user,
				'Nnew':		Nnew,
				'Gnew':		Gnew,
				'Books':	Books,
				'News':		News_list,
				'Strips':	Strips,
				'Images':	Images,
				'Gallerys':	Gallerys,
				})
	else:
		return HttpResponseRedirect(reverse('athenaeum.comic.views.login'))

def upload (request, strip_id = 0):

	if (request.session.get('User') != None):
		Nnew = new_news()
		Gnew = new_gallery()
		user = logged_in(request)
		progress = None

		curbook = None
		Books = Book.objects.order_by('-id')
		
		comment_list = []
		
		if (strip_id != 0):
			strip = Strip.objects.get (id = strip_id)
			progress = Progress.objects.get (strip = strip)

			s_list = strip.comments.split(',')
			for cid in s_list:
				if (cid != ''):
					comment_list.append(Comment.objects.get(id=cid))
			func = "Modify"
		else:
			func = "Upload"
			strip = None

		return render_to_response('upload.html', {
				'user':			user,
				'Nnew':			Nnew,
				'Gnew':			Gnew,
				'Books':		Books,
				'curbook':		curbook,
				'comment_list':	comment_list,
				'strip':		strip,
				'Progress':		progress,
				'strip_id':		strip_id,
				'Function':		func,
				})

	else:
		return HttpResponseRedirect(reverse('athenaeum.comic.views.login'))


def upload_strip (request, strip_id):
	
	if (request.session.get('User') != None):
		strip_title = request.POST['strip_title']
		date_list = request.POST['date'].split('-')
		strip_date = datetime.date( int(date_list[0]), int(date_list[1]), int(date_list[2]))	
		strip_book = Book.objects.get(id = request.POST['book'])
		sketch = request.POST['sketch']
		ink = request.POST['ink']
		color = request.POST['color']
		letter = request.POST['letter']
		strip_image = None

		if 'image' in request.FILES:
			image = request.FILES['image']
			image_name = image['filename']
			image_path = 'images/Strips/' + image_name 
			image_fd = open (image_path, 'wb')
			image_fd.write(image['content'])
			image_fd.close
			strip_image = '/' + image_path
			os.system('convert -resize 180x233 ' + image_path + ' thumb/' + image_path)
		
		
		if (strip_id == '0'):
			strip = Strip(title = strip_title, date = strip_date, book = strip_book, image = strip_image)
			strip.id = (Strip.objects.order_by('-id')[0].id + 1)
		else:
			strip = Strip.objects.get(id = strip_id)
			strip.title = strip_title
			strip.date = strip_date
			if (strip_image != None):
				strip.image = strip_image
			strip.book = strip_book

		strip.save()

		progress = Progress.objects.filter (strip=strip)
		if (len (progress) == 0):
			progress = Progress (sketch = sketch, ink = ink, color = color, letter = letter, strip = strip)
		else:
			progress = progress[0]
			progress.sketch = sketch	
			progress.ink = ink	
			progress.color = color	
			progress.letter = letter	
		
		progress.save ()

	return HttpResponseRedirect(reverse('athenaeum.comic.views.admin'))

def news_items (request, news_id = 0):

	if (request.session.get('User') != None):
		Nnew = new_news()
		Gnew = new_gallery()
		user = logged_in(request)

		if (news_id != 0):
			news = News.objects.get(id = news_id)
			func = "Modify"
			news_date = news.date
		else:
			func = "New"
			news = None
			news_date = datetime.date.today()

		return render_to_response('news_item.html', {
				'user':			user,
				'Nnew':			Nnew,
				'Gnew':			Gnew,
				'news':			news,
				'news_date':		news_date,
				'news_id':		news_id,
				'Function':		func,
				})

	else:
		return HttpResponseRedirect(reverse('athenaeum.comic.views.login'))


def news_action (request, news_id):
	
	if (request.session.get('User') != None):
		news_title = request.POST['news_title']
		date_list = request.POST['date'].split('-')
		news_date = datetime.date( int(date_list[0]), int(date_list[1]), int(date_list[2]))	
		news_user = User.objects.get(user = request.session['User'])
		news_post = request.POST['news_post']
		
		
		if (news_id == '0'):
			news = News(title = news_title, date = news_date, user = news_user, news = news_post)
			if (len(News.objects.all()) != 0):
				news.id = (News.objects.order_by('-id')[0].id + 1)
			else:
				news.id = 1
		else:
			news = News.objects.get(id = news_id)
			news.title = news_title
			news.date = news_date
			news.user = news_user
			news.news = news_post
		
		news.save()

	return HttpResponseRedirect(reverse('athenaeum.comic.views.admin'))


def comment (request, comment_id = 0, strip_id = 0, image_id = 0):

	if (request.session.get('User') != None):
		Nnew = new_news()
		Gnew = new_gallery()
		user = logged_in(request)
		
		request.session['strip'] = strip_id
		request.session['gallery_image'] = image_id

		if (comment_id != 0):
			comment = Comment.objects.get(id = comment_id)
			func = "Modify"
		else:
			comment = None
			func = "New"

		return render_to_response('comment.html', {
				'user':			user,
				'Nnew':			Nnew,
				'Gnew':			Gnew,
				'comment':		comment,
				'comment_id':	comment_id,
				'Function':		func,
				})

	else:
		return HttpResponseRedirect(reverse('athenaeum.comic.views.login'))

def comment_action (request, comment_id):
	
	if (request.session.get('User') != None):
		comment_title = request.POST['comment_title']
		comment_post = request.POST['comment_post']
		comment_user = User.objects.get(user = request.session['User'])
		loc = ''
		id = ''
		
		if (comment_id == '0'):
			comment = Comment(title = comment_title, user = comment_user, comment = comment_post)
			if ( len(Comment.objects.all()) != 0):
				comment.id = (Comment.objects.order_by('-id')[0].id + 1)
			else:
				comment.id = 1
			comment.save()

			if (request.session.get('strip') != 0):
				strip = Strip.objects.get(id=request.session.get('strip'))
				comment.strip = strip
				strip.comments += str(comment.id) + ','
				strip.save()
				comment.save()
				loc = 'strip/'
				id = request.session['strip']	
				del request.session['strip']

			if (request.session.get('gallery_image') != 0):
				gallery_image = GalleryImage.objects.get(id=request.session.get('gallery_image'))
				comment.gallery_image = gallery_image
				gallery_image.comments += str(comment.id) + ','
				gallery_image.save()
				comment.save()
				loc = 'image/'
				id = request.session['gallery_image']	
				del request.session['gallery_image']

		else:
			comment = Comment.objects.get(id = comment_id)
			comment.title = comment_title
			comment.user = comment_user
			comment.comment = comment_post
			comment.save()

			if (comment.strip != None):	
				loc = 'strip/'
				id = str (comment.strip.id)
			
			elif (comment.gallery_image != None):
				loc = 'image/'
				id = str (comment.gallery_image.id)
		
		url = '/admin/' + loc + id

		return HttpResponseRedirect(url)
	return HttpResponseRedirect(reverse('athenaeum.comic.views.admin'))

def book (request, book_id = 0):

	if (request.session.get('User') != None):
		Nnew = new_news()
		Gnew = new_gallery()
		user = logged_in(request)

		if (book_id != 0):
			book = Book.objects.get(id = book_id)
			func = "Modify"
		else:
			book = None
			func = "New"
		Strips = Strip.objects.order_by('-id')
		curstrip = Strips[0]
		return render_to_response('book.html', {
				'user':			user,
				'Nnew':			Nnew,
				'Gnew':			Gnew,
				'book':			book,
				'book_id':		book_id,
				'Strips':		Strips,
				'curstrip':		curstrip,
				'Function':		func,
				})

	else:
		return HttpResponseRedirect(reverse('athenaeum.comic.views.login'))

def book_action (request, book_id):
	
	if (request.session.get('User') != None):
		book_name = request.POST['book_title']
		book_cover = Strip.objects.get(id = request.POST['cover_strip'])
		
		
		if (book_id == '0'):
			book = Book(name = book_name, cover = book_cover)
			book.id = (Book.objects.order_by('-id')[0].id + 1)
		else:
			book = Book.objects.get(id = book_id)
			book.name = book_name
			book.cover = book_cover
		
		book.save()

	return HttpResponseRedirect(reverse('athenaeum.comic.views.admin'))

def gallery_new (request, gallery_id = 0):

	if (request.session.get('User') != None):
		Nnew = new_news()
		Gnew = new_gallery()
		user = logged_in(request)

		if (gallery_id != 0):
			gallery = Gallery.objects.get(id = gallery_id)
			func = "Modify"
		else:
			gallery = None
			func = "New"
		return render_to_response('gallery_new.html', {
				'user':				user,
				'Nnew':				Nnew,
				'Gnew':				Gnew,
				'gallery':			gallery,
				'gallery_id':		gallery_id,
				'Function':			func,
				})

	else:
		return HttpResponseRedirect(reverse('athenaeum.comic.views.login'))

def gallery_action (request, gallery_id):
	
	if (request.session.get('User') != None):
		gallery_name = request.POST['gallery_title']
		
		
		if (gallery_id == '0'):
			gallery = Gallery (name = gallery_name)
			if (len (Gallery.objects.all()) != 0):
				gallery.id = (Gallery.objects.order_by('-id')[0].id + 1)
			else:
				gallery.id = 1
		else:
			gallery = Gallery.objects.get(id = gallery_id)
			gallery.name = gallery_name
		
		gallery.save()

	return HttpResponseRedirect(reverse('athenaeum.comic.views.admin'))

def gallery_image_upload (request, image_id = 0):

	if (request.session.get('User') != None):
		Nnew = new_news()
		Gnew = new_gallery()
		user = logged_in(request)

		curgallery = None
		Gallerys = Gallery.objects.order_by('-id')
		
		comment_list = []
		
		if (image_id != 0):
			gallery_image = GalleryImage.objects.get(id=image_id)

			gi_list = gallery_image.comments.split(',')
			for cid in gi_list:
				if (cid != ''):
					comment_list.append(Comment.objects.get(id=cid))
			func = "Modify"
		else:
			func = "Upload"
			gallery_image = None

		return render_to_response('gallery_upload.html', {
				'user':				user,
				'Nnew':				Nnew,
				'Gnew':				Gnew,
				'Gallerys':			Gallerys,
				'curgallery':		curgallery,
				'comment_list':		comment_list,
				'gallery_image':	gallery_image,
				'image_id':			image_id,
				'Function':			func,
				})

	else:
		return HttpResponseRedirect(reverse('athenaeum.comic.views.login'))


def gallery_image_action (request, image_id):
	
	if (request.session.get('User') != None):
		
		gallery_image_title = request.POST['gallery_image_title']
		date_list = request.POST['date'].split('-')
		gallery_image_date = datetime.date( int(date_list[0]), int(date_list[1]), int(date_list[2]))	
		gallery_image_gallery = Gallery.objects.get(id = request.POST['gallery'])
		gallery_image_thumb = None
		gallery_image_image = None
		
		if 'thumb' in request.FILES:
			thumb = request.FILES['thumb']
			thumb_name = thumb['filename']
			thumb_path = 'images/Gallery/Thumbnails/' + thumb_name 
			thumb_fd = open (thumb_path, 'wb')
			thumb_fd.write(thumb['content'])
			thumb_fd.close
			gallery_image_thumb = '/' + thumb_path
		
		if 'image' in request.FILES:
			image = request.FILES['image']
			image_name = image['filename']
			image_path = 'images/Gallery/' + image_name 
			image_fd = open (image_path, 'wb')
			image_fd.write(image['content'])
			image_fd.close
			gallery_image_image = '/' + image_path
		
		
		if (image_id == '0'):
			gallery_image = GalleryImage(title = gallery_image_title, date = gallery_image_date, gallery = gallery_image_gallery, thumbnail = gallery_image_thumb, image = gallery_image_image)
			if ( len(GalleryImage.objects.all()) != 0):
				gallery_image.id = (GalleryImage.objects.order_by('-id')[0].id + 1)
			else:
				gallery_image.id = 1
		else:
			gallery_image = GalleryImage.objects.get(id = image_id)
			gallery_image.title = gallery_image_title
			gallery_image.date = gallery_image_date
			if (gallery_image_image != None):
				gallery_image.image = gallery_image_image
			if (gallery_image_thumb != None):
				gallery_image.thumbnail = gallery_image_thumb
			gallery_image.gallery = gallery_image_gallery

		gallery_image.save()

	return HttpResponseRedirect(reverse('athenaeum.comic.views.admin'))


def login (request):

	Nnew = new_news()
	Gnew = new_gallery()
	user = logged_in(request)
	

	return render_to_response('login.html', {
			'user':		user,
			'Nnew':		Nnew,
			'Gnew':		Gnew,
			})

def login_action (request):
	username = request.POST['username']
	password_ptxt = request.POST['password']

	user = User.objects.get(user = username)
	password = sha.new(user.salt + password_ptxt).hexdigest()

	if (password == user.password):
		request.session['User'] = user.user
		return HttpResponseRedirect(reverse('athenaeum.comic.views.index'))
	
	else:
		return HttpResponseRedirect(reverse('athenaeum.comic.views.login'))
		

def logout (request):

	Nnew = new_news()
	Gnew = new_gallery()

	del request.session['User']

	user = logged_in(request)

	return HttpResponseRedirect(reverse('athenaeum.comic.views.index'))

def delcomment (request, comment_id):
	if (request.session.get('User') != None):
		comment = Comment.objects.get(id = comment_id)
		loc = ''
		id = ''
		
		if (comment.strip != None):	
			comments_list = comment.strip.comments.split(str(comment.id) + ',')
			comment.strip.comments = comments_list[0] + comments_list[1]
			comment.strip.save()
			comment.delete()
			loc = 'strip/'
			id = str (comment.strip.id)
		
		elif (comment.gallery_image != None):
			comments_list = comment.gallery_image.comments.split(str(comment.id) + ',')
			comment.gallery_image.comments = comments_list[0] + comments_list[1]
			comment.gallery_image.save()
			comment.delete()
			loc = 'image/'
			id = str (comment.gallery_image.id)

		url = '/admin/' + loc + id
		
	return HttpResponseRedirect(url)

def delgallery (request, gallery_id):
	if (request.session.get('User') != None):
		image_list = GalleryImage.objects.filter(gallery = Gallery.objects.get(id = gallery_id))
		if (len(image_list) == 0):
			Gallery.objects.get(id = gallery_id).delete()
	return HttpResponseRedirect(reverse('athenaeum.comic.views.admin'))

def delimage (request, image_id):
	if (request.session.get('User') != None):
		GalleryImage.objects.get(id = image_id).delete()
	return HttpResponseRedirect(reverse('athenaeum.comic.views.admin'))

def delbook (request, book_id):
	if (request.session.get('User') != None):
		strip_list = Strip.objects.filter(book = Book.objects.get(id = book_id))
		if (len(strip_list) == 0):
			Book.objects.get(id = book_id).delete()
	return HttpResponseRedirect(reverse('athenaeum.comic.views.admin'))

def delnews (request, news_id):
	if (request.session.get('User') != None):
		News.objects.get(id = news_id).delete()
	return HttpResponseRedirect(reverse('athenaeum.comic.views.admin'))

def delstrip (strip_id):
	Strip.objects.get(id = strip_id).delete()

def new_news ():
	if (len (News.objects.all()) != 0):
		news = News.objects.order_by('-id')[0]
		timedelta = datetime.timedelta(days = 3)
		if (datetime.date.today() - news.date <= timedelta):
			return "new"
	else:
		return ""

def new_gallery ():
	if (len (GalleryImage.objects.all()) != 0):
		image = GalleryImage.objects.order_by('-id')[0]
		timedelta = datetime.timedelta(days = 3)
		if (datetime.date.today() - image.date <= timedelta):
			return "new"
	else:
		return ""

def logged_in (request):
	return request.session.get('User')

