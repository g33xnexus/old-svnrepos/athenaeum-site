from django.db import models

class Ad (models.Model):
	image = models.ImageField (upload_to = "/images/AdBanners")
	link = models.TextField()
	def __unicode__(self):
		return self.link

class User (models.Model):
	user = models.CharField (max_length = 255)
	avatar = models.ImageField (upload_to = "/images/Avatars", blank = True, null = True)
	nickname = models.CharField (max_length = 100)
	salt = models.CharField (max_length = 20)
	password = models.CharField (max_length = 40)
	def __unicode__(self):
		return self.user

class News (models.Model):
	user = models.ForeignKey (User)
	news = models.TextField ()
	title = models.CharField (max_length = 255)
	date = models.DateField ()
	
	def __unicode__(self):
		return self.title

class Comment (models.Model):
	user = models.ForeignKey (User)
	comment = models.TextField ()
	title = models.CharField (max_length = 255)
	strip = models.ForeignKey ("Strip", blank = True, null = True, related_name = "strip")
	gallery_image = models.ForeignKey ("Strip", blank = True, null = True, related_name = "gallery_image")
		
	def __unicode__(self):
		return self.title

class Book (models.Model):
	name = models.CharField (max_length = 255)
	cover = models.ForeignKey ("Strip", blank = True, null = True, related_name = "cover")
	
	def __unicode__(self):
		return self.name


class Strip (models.Model):
	image = models.ImageField (upload_to = "/images/Strips", blank = True, null = True)
	book = models.ForeignKey (Book)
	date = models.DateField ()
	title = models.CharField (max_length = 255)
	comments = models.CommaSeparatedIntegerField (max_length = 255)

	def __unicode__(self):
		return str (self.id)

class Gallery (models.Model):
	name = models.CharField (max_length = 255)
	
	def __unicode__(self):
		return self.name

class GalleryImage (models.Model):
	gallery = models.ForeignKey (Gallery)
	title = models.CharField (max_length = 40)
	date = models.DateField ()
	thumbnail = models.ImageField (upload_to = "/images/Gallery")
	image = models.ImageField (upload_to = "/images/Gallery")
	comments = models.CommaSeparatedIntegerField (max_length = 255)
	
	def __unicode__(self):
		return self.title

class Progress (models.Model):
	sketch = models.IntegerField()
	ink = models.IntegerField() 
	color = models.IntegerField() 
	letter = models.IntegerField() 
	strip = models.ForeignKey ("Strip")
 
