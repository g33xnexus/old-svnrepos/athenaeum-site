from athenaeum.comic.models import Book, Strip, User, Comment, News, Gallery, GalleryImage
import datetime, random, sha
b = Book (name = "Only in Dreams")
b.save()
s = Strip (book = b, image = "/images/Strips/Cover0.png", date = datetime.date (2007, 11, 21), title = "Only in Dreams")
s.save()
b.cover = s
b.save()
s = Strip (book = b, image = "/images/Strips/Strip1.png", date = datetime.date (2007, 11, 22), title = "Brave New World")
s.save()
s = Strip (book = b, image = "/images/Strips/Strip2.png", date = datetime.date (2007, 11, 23), title = "Creative Asylum")
s.save()
usalt = hex(random.getrandbits(80))[2:22]
upassword = sha.new(usalt + "archigos").hexdigest()
u1 = User (user = "morgul", nickname = "Chris", avatar="/images/Avatars/Chris.png", salt=usalt, password=upassword)
u1.save()
usalt = hex(random.getrandbits(80))[2:22]
upassword = sha.new(usalt + "imaelf8").hexdigest()
u = User (user = "emerwyn", nickname = "Ginny", avatar="/images/Avatars/Ginny.png", salt=usalt, password=upassword)
u.save()
